import ansi

THEME = {"screen_fg": "white",
         "screen_bg": "black",
         "title": {"fg": "bright_yellow", "underline": True},
         "continue": {"fg": "bright_yellow", "bg": "magenta"}}


def initialize(title=None):
    ansi.set_default_fg(THEME["screen_fg"])
    ansi.set_default_bg(THEME["screen_bg"])
    ansi.ansi_print(ansi.highlight("", fg=THEME["screen_fg"], bg=THEME["screen_bg"]))
    ansi.clear_screen(home=True)
    if title is not None:
        print(ansi.highlight(text=title, **THEME["title"]))
        print()


def finalize():
    cols, rows = ansi.get_terminal_size()
    ansi.locate(0, rows-1)
    continue_msg = "Press ENTER to continue..."
    input(ansi.highlight(continue_msg, **THEME["continue"]))
    ansi.reset()
    ansi.ansi_print(ansi.highlight("", fg=THEME["screen_fg"], bg=THEME["screen_bg"]))
    ansi.clear_screen(home=True)
