from common import initialize, finalize
from ansi import highlight, Attribute

THEME = {"column_heading": {"fg": "bright_green", "underline": True}}


def main():
    initialize("ANSI Attributes - By Name")
    display_headings()
    display_attributes()
    finalize()


def display_headings():
    col_txt = "Attribute Name    Alternative Names"
    print(highlight(col_txt, **THEME["column_heading"]))


def display_attributes():
    # for attr_name, attr_enum in Attribute.__members__.items():
    for attr_enum in Attribute:
        # The main attribute name
        name = f"{attr_enum.name: <15}"
        name_lower = attr_enum.name.lower()
        attr_dict = {name_lower: True}
        row_txt = highlight(name, **attr_dict)
        # alternative names
        row_txt += "   "
        alt_names = _get_alternative_names(attr_enum)
        row_txt += alt_names
        print(row_txt)


def _get_alternative_names(attr_enum: Attribute) -> str:
    attr_list = []
    for a_name, a_enum in Attribute.__members__.items():
        if (a_enum == attr_enum) and (a_name != attr_enum.name):
            attr_list.append(a_name)
    return ", ".join(attr_list)


if __name__ == "__main__":
    main()
