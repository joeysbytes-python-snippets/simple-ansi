from common import initialize, finalize
from ansi import highlight

THEME = {"column_heading": {"fg": "black", "bg": "cyan"},
         "row_heading": {"fg": "black", "bg": "cyan"},
         "column_indexes": {"fg": "white"},
         "row_indexes": {"fg": "white"}
         }


def main():
    initialize("ANSI Color Chart - By Index")
    display_column_heading()
    display_column_indexes()
    display_data_rows()
    finalize()


def display_column_heading():
    hdg = " " * 13
    hdg += highlight("F o r e g r o u n d   I n d e x", **THEME["column_heading"])
    print(hdg)


def display_column_indexes():
    hdg_idx = " " * 5
    for fg in range(0, 10):
        hdg_idx += f" {fg} "
    for fg in range(10, 16):
        hdg_idx += f" {fg}"
    print(highlight(hdg_idx, **THEME["column_indexes"]))


def display_data_rows():
    display_chars = " # "
    row_hdg = "Background Index"
    for bg in range(0, 16):
        row_txt = highlight(row_hdg[bg:bg+1], **THEME["row_heading"])
        row_txt += " "
        row_txt += highlight(f"{bg: >2}", **THEME["row_indexes"])
        row_txt += " "
        for fg in range(0, 16):
            row_txt += highlight(f"{display_chars}", fg=fg, bg=bg)
        print(row_txt)


if __name__ == "__main__":
    main()
