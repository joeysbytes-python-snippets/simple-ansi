from common import initialize, finalize
from ansi import highlight, Color

THEME = {"column_heading": {"fg": "bright_green", "underline": True}}


def main():
    initialize("ANSI Colors - By Name")
    display_headings()
    display_colors()
    finalize()


def display_headings():
    col_txt = "Idx  Foreground       Background"
    print(highlight(col_txt, **THEME["column_heading"]))


def display_colors():
    for color_name, color_enum in Color.__members__.items():
        fg = "black" if color_enum >= 1 else "white"
        bg = "bright_black" if color_enum == 0 else None
        # index column
        idx = f"{color_enum.index: >2}"
        row_txt = highlight(f"{idx}", fg=color_enum, bg=bg)
        # foreground column
        row_txt += "   "
        name = f"{color_name: <15}"
        row_txt += highlight(f"{name}", fg=color_enum, bg=bg)
        # background column
        row_txt += "  "
        row_txt += highlight(f"{name}", fg=fg, bg=color_enum)
        print(row_txt)


if __name__ == "__main__":
    main()
