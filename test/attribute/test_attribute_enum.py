import pytest
from ansi import Attribute, Constant
from .. import data


@pytest.mark.parametrize("attribute_data", data.ATTRIBUTES_DATA)
def test_on_index(attribute_data):
    expected = attribute_data["on"]
    actual = Attribute[attribute_data["name"]].on_index
    assert actual == expected


@pytest.mark.parametrize("attribute_data", data.ATTRIBUTES_DATA)
def test_off_index(attribute_data):
    expected = attribute_data["off"]
    actual = Attribute[attribute_data["name"]].off_index
    assert actual == expected


@pytest.mark.parametrize("attribute_data", data.ATTRIBUTES_DATA)
def test_intensity(attribute_data):
    expected = attribute_data["intensity"]
    actual = Attribute[attribute_data["name"]].intensity
    assert actual == expected


@pytest.mark.parametrize("attribute_data", data.ATTRIBUTES_DATA)
def test_on_code(attribute_data):
    intensity = attribute_data["intensity"]
    expected: str
    if intensity:
        expected = Constant.CSI.value + str(Attribute.NORMAL.on_index) + ";" + str(attribute_data["on"]) + "m"
    else:
        expected = Constant.CSI.value + str(attribute_data["on"]) + "m"
    actual = Attribute[attribute_data["name"]].on_code
    assert actual == expected


@pytest.mark.parametrize("attribute_data", data.ATTRIBUTES_DATA)
def test_off_code(attribute_data):
    expected = Constant.CSI.value + str(attribute_data["off"]) + "m"
    actual = Attribute[attribute_data["name"]].off_code
    assert actual == expected


@pytest.mark.parametrize("attribute_data", data.ATTRIBUTES_DATA)
def test_enum_name(attribute_data):
    expected = attribute_data["parent"]
    actual = Attribute[attribute_data["name"]].name
    assert actual == expected


def test_attribute_length():
    expected = len(data.ATTRIBUTES_DATA)
    actual = len(Attribute.__members__.items())
    assert actual == expected


def test_non_duplicate_length():
    expected = len(data.ATTRIBUTES_PARENTS)
    actual = len(Attribute)
    assert actual == expected
