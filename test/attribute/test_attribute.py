import pytest
import ansi
from ansi import Attribute, Constant
from ansi import highlight
from .. import data


def test_ansi_disabled():
    ansi.disable()
    expected = data.TEXT
    actual = highlight(data.TEXT, underline=True)
    assert actual == expected
    ansi.reset()


def test_no_attributes():
    expected = data.TEXT
    actual = highlight(data.TEXT, bold=False, dim=False, normal=False, italic=False, underline=False,
                       blink=False, reverse=False, conceal=False, strikethrough=False)
    assert actual == expected


@pytest.mark.parametrize("attr_name", data.ATTRIBUTES_PARENTS)
def test_single_attribute(attr_name):
    attr_enum = Attribute[attr_name]
    expected: str
    if attr_enum.intensity:
        expected = (Constant.CSI.value + str(Attribute.NORMAL.on_index) + ';' + str(attr_enum.on_index) + "m" +
                    data.TEXT + Constant.CSI.value + str(attr_enum.off_index) + "m")
    else:
        expected = (Constant.CSI.value + str(attr_enum.on_index) + "m" + data.TEXT +
                    Constant.CSI.value + str(attr_enum.off_index) + "m")
    attr_name_lower = attr_name.lower()
    kwargs = {attr_name_lower: True}
    actual = highlight(data.TEXT, **kwargs)
    assert actual == expected


@pytest.mark.parametrize("intensity_data", data.ATTRIBUTES_INTENSITY_ORDER)
def test_intensity_order(intensity_data):
    attr_enum = Attribute[intensity_data["priority"]]
    expected: str
    if attr_enum.intensity:
        expected = (Constant.CSI.value + str(Attribute.NORMAL.on_index) + ';' + str(attr_enum.on_index) + "m" +
                    data.TEXT + Constant.CSI.value + str(attr_enum.off_index) + "m")
    else:
        expected = (Constant.CSI.value + str(attr_enum.on_index) + "m" + data.TEXT +
                    Constant.CSI.value + str(attr_enum.off_index) + "m")
    attr_dict = {"normal": intensity_data["normal"],
                 "bold": intensity_data["bold"],
                 "dim": intensity_data["dim"]}
    actual = highlight(data.TEXT, **attr_dict)
    assert actual == expected
