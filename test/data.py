import ansi

TEXT = "ANSI Unit Test"


########################################################################################################################
# Config / Constants
########################################################################################################################

CONSTANTS_DATA = [
    {"name": "ESCAPE", "value": "\033"},
    {"name": "ESC", "value": "\033"},
    {"name": "CSI", "value": "\033" + "["},
    {"name": "MIN_COLOR", "value": -1},
    {"name": "MAX_COLOR", "value": 15},
    {"name": "MIN_HEX_COLOR", "value": -1},
    {"name": "MAX_HEX_COLOR", "value": 255},
    {"name": "MIN_MOVEMENT", "value": 1},
    {"name": "MIN_X_COORDINATE", "value": 0},
    {"name": "MIN_Y_COORDINATE", "value": 0},
    {"name": "MIN_HEX_CODE_INT_CHAR_LENGTH", "value": 1},
    {"name": "MAX_HEX_CODE_INT_CHAR_LENGTH", "value": 2},
    {"name": "HEX_CODE_RGB_CHAR_LENGTH", "value": 6}
]

CONFIG_DEFAULTS = [
    {"key": "disabled", "value": False},
    {"key": "default_fg", "value": ansi.Color.DEFAULT},
    {"key": "default_bg", "value": ansi.Color.DEFAULT}
]


########################################################################################################################
# Colors
########################################################################################################################

COLORS_DATA = [
    {"name": "DEFAULT", "index": -1, "fg": 39, "bg": 49, "parent": "DEFAULT"},
    {"name": "BLACK", "index": 0, "fg": 30, "bg": 40, "parent": "BLACK"},
    {"name": "RED", "index": 1, "fg": 31, "bg": 41, "parent": "RED"},
    {"name": "GREEN", "index": 2, "fg": 32, "bg": 42, "parent": "GREEN"},
    {"name": "YELLOW", "index": 3, "fg": 33, "bg": 43, "parent": "YELLOW"},
    {"name": "BLUE", "index": 4, "fg": 34, "bg": 44, "parent": "BLUE"},
    {"name": "MAGENTA", "index": 5, "fg": 35, "bg": 45, "parent": "MAGENTA"},
    {"name": "CYAN", "index": 6, "fg": 36, "bg": 46, "parent": "CYAN"},
    {"name": "WHITE", "index": 7, "fg": 37, "bg": 47, "parent": "WHITE"},

    {"name": "BRIGHT_BLACK", "index": 8, "fg": 90, "bg": 100, "parent": "BRIGHT_BLACK"},
    {"name": "GRAY", "index": 8, "fg": 90, "bg": 100, "parent": "BRIGHT_BLACK"},
    {"name": "GREY", "index": 8, "fg": 90, "bg": 100, "parent": "BRIGHT_BLACK"},
    {"name": "BRIGHT_RED", "index": 9, "fg": 91, "bg": 101, "parent": "BRIGHT_RED"},
    {"name": "BRIGHT_GREEN", "index": 10, "fg": 92, "bg": 102, "parent": "BRIGHT_GREEN"},
    {"name": "BRIGHT_YELLOW", "index": 11, "fg": 93, "bg": 103, "parent": "BRIGHT_YELLOW"},
    {"name": "BRIGHT_BLUE", "index": 12, "fg": 94, "bg": 104, "parent": "BRIGHT_BLUE"},
    {"name": "BRIGHT_MAGENTA", "index": 13, "fg": 95, "bg": 105, "parent": "BRIGHT_MAGENTA"},
    {"name": "BRIGHT_CYAN", "index": 14, "fg": 96, "bg": 106, "parent": "BRIGHT_CYAN"},
    {"name": "BRIGHT_WHITE", "index": 15, "fg": 97, "bg": 107, "parent": "BRIGHT_WHITE"},
]

COLORS_PARENTS = set([c["parent"] for c in COLORS_DATA])

COLOR_TEST_DEFAULT_FOREGROUND = ansi.Color.MAGENTA
COLOR_TEST_DEFAULT_BACKGROUND = ansi.Color.BRIGHT_GREEN
COLOR_GREEN_TYPE_MIX = [ansi.Color.GREEN, 2, "Green"]
COLOR_CYAN_TYPE_MIX = [ansi.Color.CYAN, 6, "Cyan"]
COLOR_INVALID_INTS = [ansi.Constant.MIN_COLOR.value - 1, ansi.Constant.MAX_COLOR.value + 1]
COLOR_INVALID_NAMES = ["", " ", "clear", None, 3.14, -5.12, None]


########################################################################################################################
# Attributes
########################################################################################################################

ATTRIBUTES_DATA = [
    {"name": "BOLD", "on": 1, "off": 22, "intensity": True, "parent": "BOLD"},
    {"name": "BRIGHT", "on": 1, "off": 22, "intensity": True, "parent": "BOLD"},
    {"name": "DIM", "on": 2, "off": 22, "intensity": True, "parent": "DIM"},
    {"name": "FAINT", "on": 2, "off": 22, "intensity": True, "parent": "DIM"},
    {"name": "NORMAL", "on": 22, "off": 22, "intensity": False, "parent": "NORMAL"},
    {"name": "ITALIC", "on": 3, "off": 23, "intensity": False, "parent": "ITALIC"},
    {"name": "UNDERLINE", "on": 4, "off": 24, "intensity": False, "parent": "UNDERLINE"},
    {"name": "BLINK", "on": 5, "off": 25, "intensity": False, "parent": "BLINK"},
    {"name": "FLASH", "on": 5, "off": 25, "intensity": False, "parent": "BLINK"},
    {"name": "REVERSE", "on": 7, "off": 27, "intensity": False, "parent": "REVERSE"},
    {"name": "CONCEAL", "on": 8, "off": 28, "intensity": False, "parent": "CONCEAL"},
    {"name": "HIDE", "on": 8, "off": 28, "intensity": False, "parent": "CONCEAL"},
    {"name": "STRIKETHROUGH", "on": 9, "off": 29, "intensity": False, "parent": "STRIKETHROUGH"},
    {"name": "STRIKETHRU", "on": 9, "off": 29, "intensity": False, "parent": "STRIKETHROUGH"},
    {"name": "STRIKEOUT", "on": 9, "off": 29, "intensity": False, "parent": "STRIKETHROUGH"},
    {"name": "CROSSTHROUGH", "on": 9, "off": 29, "intensity": False, "parent": "STRIKETHROUGH"},
    {"name": "CROSSTHRU", "on": 9, "off": 29, "intensity": False, "parent": "STRIKETHROUGH"},
    {"name": "CROSSOUT", "on": 9, "off": 29, "intensity": False, "parent": "STRIKETHROUGH"}
]

ATTRIBUTES_PARENTS = set([a["parent"] for a in ATTRIBUTES_DATA])

ATTRIBUTES_INTENSITY_ORDER = [
    {"normal": True, "bold": True, "dim": True, "priority": "NORMAL"},
    {"normal": True, "bold": False, "dim": True, "priority": "NORMAL"},
    {"normal": True, "bold": True, "dim": False, "priority": "NORMAL"},
    {"normal": True, "bold": False, "dim": False, "priority": "NORMAL"},
    {"normal": False, "bold": True, "dim": True, "priority": "BOLD"},
    {"normal": False, "bold": True, "dim": False, "priority": "BOLD"},
    {"normal": False, "bold": False, "dim": True, "priority": "DIM"}
]


########################################################################################################################
# Cursor
########################################################################################################################

CURSOR_DATA = [
    {"name": "UP", "template": "\033[{}A", "parent": "UP"},
    {"name": "DOWN", "template": "\033[{}B", "parent": "DOWN"},
    {"name": "RIGHT", "template": "\033[{}C", "parent": "RIGHT"},
    {"name": "LEFT", "template": "\033[{}D", "parent": "LEFT"},
    {"name": "NEXT_LINE", "template": "\033[{}E", "parent": "NEXT_LINE"},
    {"name": "PREV_LINE", "template": "\033[{}F", "parent": "PREV_LINE"},
    {"name": "PREVIOUS_LINE", "template": "\033[{}F", "parent": "PREV_LINE"},
    {"name": "LOCATE_COLUMN", "template": "\033[{}G", "parent": "LOCATE_COLUMN"},
    {"name": "LOCATE", "template": "\033[{};{}H", "parent": "LOCATE"}
]

CURSOR_PARENTS = set(c["parent"] for c in CURSOR_DATA)


########################################################################################################################
# Clear
########################################################################################################################

CLEAR_DATA = [
    {"name": "SCREEN", "code": "\033[2J", "parent": "SCREEN"},
    {"name": "TO_END_OF_SCREEN", "code": "\033[0J", "parent": "TO_END_OF_SCREEN"},
    {"name": "TO_BEGIN_OF_SCREEN", "code": "\033[1J", "parent": "TO_BEGIN_OF_SCREEN"},
    {"name": "LINE", "code": "\033[2K", "parent": "LINE"},
    {"name": "TO_END_OF_LINE", "code": "\033[0K", "parent": "TO_END_OF_LINE"},
    {"name": "TO_BEGIN_OF_LINE", "code": "\033[1K", "parent": "TO_BEGIN_OF_LINE"},
]

CLEAR_PARENTS = set(c["parent"] for c in CLEAR_DATA)
CLEAR_ARROWS = ["<", ">", "*"]


########################################################################################################################
# Hex to Int, Hex to RGB
########################################################################################################################

VALID_HEX_CODES = [("0", 0), ("3", 3), ("#a", 10), ("F", 15), ("# Ab", 171), (" 33 ", 51)]
# these have valid lengths
INVALID_HEX_CODES = ["g", " ", "3g", "  ", "$", "<>"]
INVALID_HEX_CODE_BY_LENGTH = ["", "abc"]
INVALID_HEX_CODE_BY_TYPE = [-3, 0, 2, 3.14, 0.0, -5.67, None, True]
