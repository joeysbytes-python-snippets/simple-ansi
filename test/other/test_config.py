import pytest

import ansi
from ansi import Color, CONFIG
from .. import data


def test_config_length():
    expected = len(data.CONFIG_DEFAULTS)
    actual = len(CONFIG)
    assert actual == expected


@pytest.mark.parametrize("config_data", data.CONFIG_DEFAULTS)
def test_defaults(config_data):
    assert CONFIG[config_data["key"]] == config_data["value"]


def test_enable_disable():
    ansi.disable()
    assert CONFIG["disabled"] is True
    assert ansi.is_enabled() is False
    assert ansi.is_disabled() is True
    ansi.enable()
    assert CONFIG["disabled"] is False
    assert ansi.is_enabled() is True
    assert ansi.is_disabled() is False


def test_reset():
    ansi.disable()
    ansi.set_default_fg(Color.BLUE)
    ansi.set_default_bg(Color.YELLOW)
    ansi.reset()
    assert ansi.is_enabled() is True
    assert ansi.get_default_fg() == Color.DEFAULT
    assert ansi.get_default_bg() == Color.DEFAULT


@pytest.mark.parametrize("color_value", data.COLOR_GREEN_TYPE_MIX)
def test_set_default_fg(color_value):
    expected = Color.GREEN
    ansi.set_default_fg(color_value)
    actual = ansi.get_default_fg()
    assert actual == expected
    assert CONFIG["default_fg"] == expected
    ansi.set_default_fg(Color.DEFAULT)
    assert Color.DEFAULT == ansi.get_default_fg()


@pytest.mark.parametrize("color_value", data.COLOR_GREEN_TYPE_MIX)
def test_set_default_colors_fg(color_value):
    expected = Color.GREEN
    ansi.set_default_colors(color_value)
    actual = ansi.get_default_fg()
    assert actual == expected
    assert CONFIG["default_fg"] == expected
    ansi.set_default_colors(fg=Color.DEFAULT)
    assert Color.DEFAULT == ansi.get_default_fg()


@pytest.mark.parametrize("color_value", data.COLOR_GREEN_TYPE_MIX)
def test_set_default_bg(color_value):
    expected = Color.GREEN
    ansi.set_default_bg(color_value)
    actual = ansi.get_default_bg()
    assert actual == expected
    assert CONFIG["default_bg"] == expected
    ansi.set_default_bg(Color.DEFAULT)
    assert Color.DEFAULT == ansi.get_default_bg()


@pytest.mark.parametrize("color_value", data.COLOR_GREEN_TYPE_MIX)
def test_set_default_colors_bg(color_value):
    expected = Color.GREEN
    ansi.set_default_colors(None, color_value)
    actual = ansi.get_default_bg()
    assert actual == expected
    assert CONFIG["default_bg"] == expected
    ansi.set_default_colors(bg=Color.DEFAULT)
    assert Color.DEFAULT == ansi.get_default_bg()


def test_set_default_colors_no_change():
    expected = Color.DEFAULT
    ansi.set_default_colors()
    assert ansi.get_default_fg() == expected
    assert ansi.get_default_bg() == expected


def test_set_default_colors_fg_and_bg():
    expected_fg = Color.GREEN
    expected_bg = Color.CYAN
    ansi.set_default_colors(expected_fg, expected_bg)
    actual_fg = ansi.get_default_fg()
    actual_bg = ansi.get_default_bg()
    assert actual_fg == expected_fg
    assert actual_bg == expected_bg
    ansi.set_default_colors(Color.DEFAULT, Color.DEFAULT)
    assert ansi.get_default_fg() == Color.DEFAULT
    assert ansi.get_default_bg() == Color.DEFAULT
