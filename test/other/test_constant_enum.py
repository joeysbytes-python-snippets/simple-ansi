import pytest

from ansi import Constant
from .. import data


@pytest.mark.parametrize("constants_data", data.CONSTANTS_DATA)
def test_constants_value(constants_data):
    expected = constants_data["value"]
    constant = Constant[constants_data["name"]]
    actual = constant.value
    assert actual == expected


def test_constants_length():
    expected = len(data.CONSTANTS_DATA)
    actual = len(Constant.__members__.items())
    assert actual == expected
