from ansi import Clear, Constant
from ansi import clear_line_code, clear_line


def test_clear_line_code_forward():
    arrow = ">"
    expected = Constant.CSI.value + "0K"
    actual = clear_line_code(arrow)
    assert actual == expected


def test_clear_line_code_forward_force_home():
    arrow = ">"
    expected = Constant.CSI.value + "0K" + Constant.CSI.value + "1G"
    actual = clear_line_code(arrow, True)
    assert actual == expected


def test_clear_line_code_backward():
    arrow = "<"
    expected = Constant.CSI.value + "1K"
    actual = clear_line_code(arrow)
    assert actual == expected


def test_clear_line_code_backward_force_home():
    arrow = "<"
    expected = Constant.CSI.value + "1K" + Constant.CSI.value + "1G"
    actual = clear_line_code(arrow, True)
    assert actual == expected


def test_clear_line_code_all():
    arrow = "*"
    expected = Constant.CSI.value + "2K"
    actual = clear_line_code(arrow)
    assert actual == expected


def test_clear_line_code_defaults():
    expected = Constant.CSI.value + "2K"
    actual = clear_line_code()
    assert actual == expected


def test_clear_line_code_all_force_home():
    arrow = "*"
    expected = Constant.CSI.value + "2K" + Constant.CSI.value + "1G"
    actual = clear_line_code(arrow, True)
    assert actual == expected


def test_clear_line(capsys):
    arrow = "<"
    expected = Constant.CSI.value + "1K"
    clear_line(arrow)
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
    assert captured.err == ""


def test_clear_line_force_home(capsys):
    arrow = ">"
    expected = Constant.CSI.value + "0K" + Constant.CSI.value + "1G"
    clear_line(arrow, True)
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
    assert captured.err == ""


def test_clear_line_defaults(capsys):
    expected = Constant.CSI.value + "2K"
    clear_line()
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
    assert captured.err == ""
    assert actual == expected
