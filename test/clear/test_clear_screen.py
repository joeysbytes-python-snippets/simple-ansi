from ansi import Clear, Constant
from ansi import clear_screen_code, clear_screen


def test_clear_screen_code_forward():
    arrow = ">"
    expected = Constant.CSI.value + "0J"
    actual = clear_screen_code(arrow)
    assert actual == expected


def test_clear_screen_code_forward_force_home():
    arrow = ">"
    expected = Constant.CSI.value + "0J" + Constant.CSI.value + "1;1H"
    actual = clear_screen_code(arrow, True)
    assert actual == expected


def test_clear_screen_code_backward():
    arrow = "<"
    expected = Constant.CSI.value + "1J"
    actual = clear_screen_code(arrow)
    assert actual == expected


def test_clear_screen_code_backward_force_home():
    arrow = "<"
    expected = Constant.CSI.value + "1J" + Constant.CSI.value + "1;1H"
    actual = clear_screen_code(arrow, True)
    assert actual == expected


def test_clear_screen_code_all():
    arrow = "*"
    expected = Constant.CSI.value + "2J"
    actual = clear_screen_code(arrow)
    assert actual == expected


def test_clear_screen_code_defaults():
    expected = Constant.CSI.value + "2J"
    actual = clear_screen_code()
    assert actual == expected


def test_clear_screen_code_all_force_home():
    arrow = "*"
    expected = Constant.CSI.value + "2J" + Constant.CSI.value + "1;1H"
    actual = clear_screen_code(arrow, True)
    assert actual == expected


def test_clear_screen(capsys):
    arrow = "<"
    expected = Constant.CSI.value + "1J"
    clear_screen(arrow)
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
    assert captured.err == ""


def test_clear_screen_force_home(capsys):
    arrow = ">"
    expected = Constant.CSI.value + "0J" + Constant.CSI.value + "1;1H"
    clear_screen(arrow, True)
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
    assert captured.err == ""


def test_clear_screen_defaults(capsys):
    expected = Constant.CSI.value + "2J"
    clear_screen()
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
    assert captured.err == ""
    assert actual == expected
