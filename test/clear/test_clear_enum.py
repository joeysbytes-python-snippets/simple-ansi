import pytest
from ansi import Clear
from .. import data


@pytest.mark.parametrize("clear_data", data.CLEAR_DATA)
def test_code(clear_data):
    expected = clear_data["code"]
    actual = Clear[clear_data["name"]].code
    assert actual == expected


def test_clear_length():
    expected = len(data.CLEAR_DATA)
    actual = len(Clear.__members__.items())
    assert actual == expected


def test_clear_non_duplicate_length():
    expected = len(data.CLEAR_PARENTS)
    actual = len(Clear)
    assert actual == expected
