from ansi import Constant
from ansi import move_code, move


def test_move_code_up():
    distance = -5
    expected = Constant.CSI.value + str(abs(distance)) + "A"
    actual = move_code(0, distance)
    assert actual == expected


def test_move_code_up_begin_line():
    distance = -5
    expected = Constant.CSI.value + str(abs(distance)) + "F"
    actual = move_code(0, distance, True)
    assert actual == expected


def test_move_code_down():
    distance = 5
    expected = Constant.CSI.value + str(abs(distance)) + "B"
    actual = move_code(0, distance)
    assert actual == expected


def test_move_code_down_begin_line():
    distance = 5
    expected = Constant.CSI.value + str(abs(distance)) + "E"
    actual = move_code(0, distance, True)
    assert actual == expected


def test_move_code_left():
    distance = -5
    expected = Constant.CSI.value + str(abs(distance)) + "D"
    actual = move_code(distance, 0)
    assert actual == expected


def test_move_code_left_begin_line():
    distance = -5
    expected = Constant.CSI.value + "1G"
    actual = move_code(distance, 0, True)
    assert actual == expected


def test_move_code_right():
    distance = 5
    expected = Constant.CSI.value + str(abs(distance)) + "C"
    actual = move_code(distance, 0)
    assert actual == expected


def test_move_code_right_begin_line():
    distance = 5
    expected = Constant.CSI.value + "1G"
    actual = move_code(distance, 0, True)
    assert actual == expected


def test_move_code_diagonal_positive():
    x_distance = 3
    y_distance = 5
    expected = (Constant.CSI.value + str(abs(x_distance)) + "C" +
                Constant.CSI.value + str(abs(y_distance)) + "B")
    actual = move_code(x_distance, y_distance)
    assert actual == expected


def test_move_code_diagonal_positive_begin_line():
    x_distance = 3
    y_distance = 5
    expected = Constant.CSI.value + str(abs(y_distance)) + "E"
    actual = move_code(x_distance, y_distance, True)
    assert actual == expected


def test_move_code_diagonal_negative():
    x_distance = -3
    y_distance = -5
    expected = (Constant.CSI.value + str(abs(x_distance)) + "D" +
                Constant.CSI.value + str(abs(y_distance)) + "A")
    actual = move_code(x_distance, y_distance)
    assert actual == expected


def test_move_code_diagonal_negative_begin_line():
    x_distance = -3
    y_distance = -5
    expected = Constant.CSI.value + str(abs(y_distance)) + "F"
    actual = move_code(x_distance, y_distance, True)
    assert actual == expected


def test_move_code_nowhere():
    expected = ""
    actual = move_code(0, 0)
    assert actual == expected


def test_move_code_nowhere_begin_line():
    expected = Constant.CSI.value + "1G"
    actual = move_code(0, 0, True)
    assert actual == expected


def test_move(capsys):
    x_distance = 4
    y_distance = -2
    expected = (Constant.CSI.value + str(abs(x_distance)) + "C" +
                Constant.CSI.value + str(abs(y_distance)) + "A")
    move(x_distance, y_distance)
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
    assert captured.err == ""


def test_move_begin_line(capsys):
    x_distance = 4
    y_distance = -2
    expected = Constant.CSI.value + str(abs(y_distance)) + "F"
    move(x_distance, y_distance, True)
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
    assert captured.err == ""
