import pytest
from ansi import Cursor
from .. import data


@pytest.mark.parametrize("cursor_data", data.CURSOR_DATA)
def test_template(cursor_data):
    expected = cursor_data["template"]
    actual = Cursor[cursor_data["name"]].template
    assert actual == expected


def test_cursor_length():
    expected = len(data.CURSOR_DATA)
    actual = len(Cursor.__members__.items())
    assert actual == expected


def test_non_duplicate_length():
    expected = len(data.CURSOR_PARENTS)
    actual = len(Cursor)
    assert actual == expected
