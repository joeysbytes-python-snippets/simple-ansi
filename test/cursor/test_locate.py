import pytest
from ansi import Cursor, Constant
from ansi import locate_code, locate


@pytest.mark.parametrize("y", [2, None])
def test_locate_code_invalid_x(y):
    x = Constant.MIN_X_COORDINATE.value - 1
    expected = "Invalid location x-coordinate, x >= 0: " + str(x)
    with pytest.raises(ValueError) as ve:
        locate_code(x, y)
    actual = str(ve.value)
    assert actual == expected


def test_locate_code_invalid_y():
    y = Constant.MIN_Y_COORDINATE.value - 1
    expected = "Invalid location y-coordinate, y >= 0: " + str(y)
    with pytest.raises(ValueError) as ve:
        locate_code(2, y)
    actual = str(ve.value)
    assert actual == expected


def test_locate_code_minimal_coordinates():
    x = Constant.MIN_X_COORDINATE.value
    y = Constant.MIN_Y_COORDINATE.value
    expected = "\033[" + str(y+1) + ";" + str(x+1) + "H"
    actual = locate_code(x, y)
    assert actual == expected


def test_locate_code_value_coordinates():
    x = 5
    y = 10
    expected = "\033[" + str(y+1) + ";" + str(x+1) + "H"
    actual = locate_code(x, y)
    assert actual == expected


def test_locate_code_horizontal():
    x = 6
    y = None
    expected = "\033[" + str(x + 1) + "G"
    actual = locate_code(x, y)
    assert actual == expected


def test_locate(capsys):
    x = 3
    y = 7
    expected = "\033[" + str(y+1) + ";" + str(x+1) + "H"
    locate(x, y)
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
    assert captured.err == ""


def test_locate_horizontal(capsys):
    x = 4
    y = None
    expected = "\033[" + str(x+1) + "G"
    locate(x, y)
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
    assert captured.err == ""
