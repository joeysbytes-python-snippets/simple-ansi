from ansi import get_cursor_location


def test_get_cursor_location_zero_based(monkeypatch):
    monkeypatch.setattr("ansi._get_cursor_location_linux", lambda: "4;9")
    expected = (8, 3)
    actual = get_cursor_location()
    assert actual == expected
