import pytest
from ansi import hex_to_int
from ansi import Constant
from .. import data


@pytest.mark.parametrize("hex_code, int_value", data.VALID_HEX_CODES)
def test_valid_values(hex_code, int_value):
    expected = int_value
    actual = hex_to_int(hex_code)
    assert actual == expected


@pytest.mark.parametrize("hex_code", data.INVALID_HEX_CODES)
def test_invalid_values(hex_code):
    # These errors are raised by the system
    with pytest.raises(ValueError):
        hex_to_int(hex_code)


@pytest.mark.parametrize("hex_code", data.INVALID_HEX_CODE_BY_LENGTH)
def test_invalid_lengths(hex_code):
    expected = ("Invalid hex code integer length: " + str(hex_code) + " == " + str(len(hex_code)) +
                ", " + str(Constant.MIN_HEX_CODE_INT_CHAR_LENGTH.value) + " <= len(hex_code) <= " +
                str(Constant.MAX_HEX_CODE_INT_CHAR_LENGTH.value))
    with pytest.raises(ValueError) as ve:
        hex_to_int(hex_code)
    actual = str(ve.value)
    assert actual == expected


@pytest.mark.parametrize("hex_code", data.INVALID_HEX_CODE_BY_TYPE)
def test_invalid_types(hex_code):
    with pytest.raises(AttributeError):
        hex_to_int(hex_code)
