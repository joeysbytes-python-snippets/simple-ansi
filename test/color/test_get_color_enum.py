import pytest

from ansi import Color, Constant
from ansi import get_color_enum
from .. import data


@pytest.mark.parametrize("color_index", range(Constant.MIN_COLOR.value, Constant.MAX_COLOR.value + 1))
def test_string_int_valid(color_index):
    expected = Color(color_index)
    color_idx_str = str(color_index)
    actual = get_color_enum(color_idx_str)
    assert actual == expected


@pytest.mark.parametrize("color_index", range(Constant.MIN_COLOR.value, Constant.MAX_COLOR.value + 1))
def test_int_valid(color_index):
    expected = Color(color_index)
    actual = get_color_enum(color_index)
    assert actual == expected


@pytest.mark.parametrize("color_index", data.COLOR_INVALID_INTS)
def test_int_invalid(color_index):
    expected = "Invalid color index: " + str(color_index) + ", "
    expected += str(Constant.MIN_COLOR.value) + " <= color_index <= " + str(Constant.MAX_COLOR.value)
    with pytest.raises(ValueError) as ve:
        get_color_enum(color_index)
    assert str(ve.value) == expected


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_name_valid(color_data):
    expected = Color[color_data["name"]]
    actual = get_color_enum(color_data["name"])
    assert actual == expected


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_name_lowercase_valid(color_data):
    expected = Color[color_data["name"]]
    color_name = color_data["name"].lower()
    actual = get_color_enum(color_name)
    assert actual == expected


@pytest.mark.parametrize("color_name", data.COLOR_INVALID_NAMES)
def test_name_invalid(color_name):
    expected = "Invalid color name: " + str(color_name)
    with pytest.raises(ValueError) as ve:
        get_color_enum(color_name)
    assert str(ve.value) == expected
