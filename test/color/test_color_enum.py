import pytest

from ansi import Color, Constant
from .. import data


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_color_value(color_data):
    expected = color_data["index"]
    actual = Color[color_data["name"]]
    assert actual == expected
    assert actual.value == expected


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_color_index(color_data):
    expected = color_data["index"]
    actual = Color[color_data["name"]].index
    assert actual == expected


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_color_fg_index(color_data):
    expected = color_data["fg"]
    actual = Color[color_data["name"]].fg_index
    assert actual == expected


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_color_fg_code(color_data):
    expected = Constant.CSI.value + str(color_data["fg"]) + "m"
    actual = Color[color_data["name"]].fg_code
    assert actual == expected


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_color_bg_index(color_data):
    expected = color_data["bg"]
    actual = Color[color_data["name"]].bg_index
    assert actual == expected


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_color_bg_code(color_data):
    expected = Constant.CSI.value + str(color_data["bg"]) + "m"
    actual = Color[color_data["name"]].bg_code
    assert actual == expected


def test_color_length():
    expected = len(data.COLORS_DATA)
    actual = len(Color.__members__.items())
    assert actual == expected


def test_non_duplicate_length():
    expected = len(data.COLORS_PARENTS)
    actual = len(Color)
    assert actual == expected
