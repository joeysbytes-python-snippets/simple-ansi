import pytest

import ansi
from ansi import Color, Constant
from ansi import highlight
from .. import data


@pytest.fixture(autouse=True)
def setup_and_teardown():
    ansi.reset()
    ansi.set_default_fg(data.COLOR_TEST_DEFAULT_FOREGROUND)
    ansi.set_default_bg(data.COLOR_TEST_DEFAULT_BACKGROUND)
    yield
    ansi.reset()


def test_disabled():
    ansi.disable()
    expected = data.TEXT
    actual = highlight(data.TEXT, 0, 1)
    assert actual == expected


def test_no_colors():
    expected = data.TEXT
    actual = highlight(data.TEXT, None, None)
    assert actual == expected


@pytest.mark.parametrize("color_index", range(Constant.MIN_COLOR.value, Constant.MAX_COLOR.value + 1))
def test_fg_valid_int(color_index):
    expected = Color(color_index).fg_code + data.TEXT + data.COLOR_TEST_DEFAULT_FOREGROUND.fg_code
    actual = highlight(data.TEXT, color_index)
    assert actual == expected


@pytest.mark.parametrize("color_index", range(Constant.MIN_COLOR.value, Constant.MAX_COLOR.value + 1))
def test_bg_valid_int(color_index):
    expected = Color(color_index).bg_code + data.TEXT + data.COLOR_TEST_DEFAULT_BACKGROUND.bg_code
    actual = highlight(data.TEXT, None, color_index)
    assert actual == expected


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_fg_valid_name(color_data):
    color_name = color_data["name"]
    expected = Color[color_name].fg_code + data.TEXT + data.COLOR_TEST_DEFAULT_FOREGROUND.fg_code
    actual = highlight(data.TEXT, color_name)
    assert actual == expected


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_bg_valid_name(color_data):
    color_name = color_data["name"]
    expected = Color[color_name].bg_code + data.TEXT + data.COLOR_TEST_DEFAULT_BACKGROUND.bg_code
    actual = highlight(data.TEXT, None, color_name)
    assert actual == expected


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_fg_valid_enum(color_data):
    color_enum = Color[color_data["name"]]
    expected = color_enum.fg_code + data.TEXT + data.COLOR_TEST_DEFAULT_FOREGROUND.fg_code
    actual = highlight(data.TEXT, color_enum)
    assert actual == expected


@pytest.mark.parametrize("color_data", data.COLORS_DATA)
def test_bg_valid_enum(color_data):
    color_enum = Color[color_data["name"]]
    expected = color_enum.bg_code + data.TEXT + data.COLOR_TEST_DEFAULT_BACKGROUND.bg_code
    actual = highlight(data.TEXT, None, color_enum)
    assert actual == expected


@pytest.mark.parametrize("fg_color", data.COLOR_GREEN_TYPE_MIX)
@pytest.mark.parametrize("bg_color", data.COLOR_CYAN_TYPE_MIX)
def test_fg_and_bg(fg_color, bg_color):
    expected = (Constant.CSI.value + str(Color.GREEN.fg_index) + ";" +
                str(Color.CYAN.bg_index) + "m" + data.TEXT +
                Constant.CSI.value + str(data.COLOR_TEST_DEFAULT_FOREGROUND.fg_index) +
                ";" + str(data.COLOR_TEST_DEFAULT_BACKGROUND.bg_index) + "m")
    actual = highlight(data.TEXT, fg_color, bg_color)
    assert actual == expected
