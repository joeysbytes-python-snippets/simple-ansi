from ansi import ansi_print
from .. import data


def test_ansi_print(capsys):
    expected = data.TEXT
    ansi_print(data.TEXT)
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
    assert captured.err == ""
