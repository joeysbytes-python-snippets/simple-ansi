from ansi import get_terminal_size


def test_get_terminal_size():
    # When Python can't get a terminal, it returns these values
    default_rows = 24
    default_cols = 80
    expected = (default_cols, default_rows)
    actual = get_terminal_size()
    assert actual == expected
