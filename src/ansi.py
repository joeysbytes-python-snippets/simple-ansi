# MIT License: https://gitlab.com/joeysbytes-python-snippets/simple-ansi/-/raw/main/LICENSE?ref_type=heads
# Copyright (c) 2024 Joey Rockhold
import shutil
import sys
import termios
import tty

# TODO: strip codes, 256 color, rgb color, hex2rgb
# TODO: Update tiny_ansi to match this

from enum import Enum, IntEnum
from typing import Union, Optional, Tuple


########################################################################################################################
# Constants
########################################################################################################################

class Constant(Enum):
    ESCAPE = '\033'
    ESC = ESCAPE
    CSI = '\033['
    MIN_COLOR = -1
    MAX_COLOR = 15
    MIN_HEX_COLOR = -1
    MAX_HEX_COLOR = 255
    MIN_MOVEMENT = 1
    MIN_X_COORDINATE = 0
    MIN_Y_COORDINATE = 0
    MIN_HEX_CODE_INT_CHAR_LENGTH = 1
    MAX_HEX_CODE_INT_CHAR_LENGTH = 2
    HEX_CODE_RGB_CHAR_LENGTH = 6


########################################################################################################################
# Color (ANSI Standard 16 Colors)
########################################################################################################################

class Color(IntEnum):
    DEFAULT = -1
    BLACK = 0
    RED = 1
    GREEN = 2
    YELLOW = 3
    BLUE = 4
    MAGENTA = 5
    CYAN = 6
    WHITE = 7
    BRIGHT_BLACK = 8
    GRAY = BRIGHT_BLACK
    GREY = BRIGHT_BLACK
    BRIGHT_RED = 9
    BRIGHT_GREEN = 10
    BRIGHT_YELLOW = 11
    BRIGHT_BLUE = 12
    BRIGHT_MAGENTA = 13
    BRIGHT_CYAN = 14
    BRIGHT_WHITE = 15

    def __init__(self, index: int):
        self.index = index

    @property
    def fg_index(self) -> int:
        return self._color_index(background=False)

    @property
    def fg_code(self) -> str:
        return f"{Constant.CSI.value}{self.fg_index}m"

    @property
    def bg_index(self) -> int:
        return self._color_index(background=True)

    @property
    def bg_code(self) -> str:
        return f"{Constant.CSI.value}{self.bg_index}m"

    def _color_index(self, background: bool) -> int:
        if self.index == -1:
            return 39 + (background * 10)
        return (self.index + 30) + ((self.index >= 8) * 52) + (background * 10)


def get_color_enum(value: Union[int, str]) -> Color:
    color_enum: Color
    # If value is a string
    if isinstance(value, str):
        try:
            # Check if value is an integer string
            value = int(value)
        except ValueError:
            pass
    # If value is an int
    if isinstance(value, int):
        color_idx = value
        if Constant.MIN_COLOR.value <= color_idx <= Constant.MAX_COLOR.value:
            color_enum = Color(color_idx)
        else:
            msg = f"Invalid color index: {color_idx}, "
            msg += f"{Constant.MIN_COLOR.value} <= color_index <= {Constant.MAX_COLOR.value}"
            raise ValueError(msg)
    else:
        # Treat value as a string name for Color enum
        color_name = str(value).upper().strip()
        try:
            color_enum = Color[color_name]
        except KeyError:
            msg = f"Invalid color name: {value}"
            raise ValueError(msg)
    return color_enum


########################################################################################################################
# Attributes
########################################################################################################################

class Attribute(Enum):
    BOLD = (1, 22, True)
    BRIGHT = BOLD
    DIM = (2, 22, True)
    FAINT = DIM
    NORMAL = (22, 22, False)
    ITALIC = (3, 23, False)
    UNDERLINE = (4, 24, False)
    BLINK = (5, 25, False)
    FLASH = BLINK
    REVERSE = (7, 27, False)
    CONCEAL = (8, 28, False)
    HIDE = CONCEAL
    STRIKETHROUGH = (9, 29, False)
    STRIKETHRU = STRIKETHROUGH
    STRIKEOUT = STRIKETHROUGH
    CROSSTHROUGH = STRIKETHROUGH
    CROSSTHRU = STRIKETHROUGH
    CROSSOUT = STRIKETHROUGH

    def __init__(self, on_index: int, off_index: int, intensity: bool):
        self.on_index = on_index
        self.off_index = off_index
        self.intensity = intensity

    @property
    def on_code(self) -> str:
        if self.intensity:
            return f"{Constant.CSI.value}{Attribute.NORMAL.on_index};{self.on_index}m"
        return f"{Constant.CSI.value}{self.on_index}m"

    @property
    def off_code(self) -> str:
        return f"{Constant.CSI.value}{self.off_index}m"


########################################################################################################################
# Highlighting
########################################################################################################################

def highlight(text: str,
              fg: Optional[Union[int, str, Color]] = None,
              bg: Optional[Union[int, str, Color]] = None,
              bold: bool = False,
              dim: bool = False,
              normal: bool = False,
              italic: bool = False,
              underline: bool = False,
              blink: bool = False,
              reverse: bool = False,
              conceal: bool = False,
              strikethrough: bool = False
              ) -> str:

    if is_disabled():
        return text

    start_codes = []
    end_codes = []

    # colors
    if fg is not None:
        fg_enum = fg if isinstance(fg, Color) else get_color_enum(fg)
        start_codes.append(str(fg_enum.fg_index))
        end_codes.append(str(get_default_fg().fg_index))
    if bg is not None:
        bg_enum = bg if isinstance(bg, Color) else get_color_enum(bg)
        start_codes.append(str(bg_enum.bg_index))
        end_codes.append(str(get_default_bg().bg_index))

    # attributes
    def _add_attribute(attr_enum: Attribute) -> None:
        if attr_enum.intensity:
            start_codes.append(str(Attribute.NORMAL.on_index))
        start_codes.append(str(attr_enum.on_index))
        end_codes.append(str(attr_enum.off_index))

    # intensity
    if normal:
        _add_attribute(Attribute.NORMAL)
    elif bold:
        _add_attribute(Attribute.BOLD)
    elif dim:
        _add_attribute(Attribute.DIM)

    # attribute flags
    if italic:
        _add_attribute(Attribute.ITALIC)
    if underline:
        _add_attribute(Attribute.UNDERLINE)
    if blink:
        _add_attribute(Attribute.BLINK)
    if reverse:
        _add_attribute(Attribute.REVERSE)
    if conceal:
        _add_attribute(Attribute.CONCEAL)
    if strikethrough:
        _add_attribute(Attribute.STRIKETHROUGH)

    # return ANSI string
    if len(start_codes) == 0:
        return text
    else:
        ansi_text = Constant.CSI.value
        ansi_text += ';'.join(start_codes)
        ansi_text += 'm'
        ansi_text += text
        ansi_text += Constant.CSI.value
        ansi_text += ';'.join(end_codes)
        ansi_text += 'm'
        return ansi_text


########################################################################################################################
# Cursor
########################################################################################################################

class Cursor(Enum):
    UP = Constant.CSI.value + "{}A"
    DOWN = Constant.CSI.value + "{}B"
    RIGHT = Constant.CSI.value + "{}C"
    LEFT = Constant.CSI.value + "{}D"
    NEXT_LINE = Constant.CSI.value + "{}E"
    PREV_LINE = Constant.CSI.value + "{}F"
    PREVIOUS_LINE = PREV_LINE
    LOCATE_COLUMN = Constant.CSI.value + "{}G"
    LOCATE = Constant.CSI.value + "{};{}H"

    def __init__(self, template: str):
        self.template = template


def move(x: int, y: int, line_begin: bool = False) -> None:
    ansi_print(move_code(x, y, line_begin))


def move_code(x: int, y: int, line_begin: bool = False) -> Optional[str]:
    codes = ""
    x_int = int(x)
    y_int = int(y)
    if not line_begin:
        # horizontal / x
        if x_int <= -Constant.MIN_MOVEMENT.value:
            codes += Cursor.LEFT.template.format(abs(x_int))
        elif x_int >= Constant.MIN_MOVEMENT.value:
            codes += Cursor.RIGHT.template.format(x_int)
        # vertical / y
        if y_int <= -Constant.MIN_MOVEMENT.value:
            codes += Cursor.UP.template.format(abs(y_int))
        elif y_int >= Constant.MIN_MOVEMENT.value:
            codes += Cursor.DOWN.template.format(y_int)
    elif y_int == 0:
        # move to beginning of current row
        codes += Cursor.LOCATE_COLUMN.template.format(Constant.MIN_X_COORDINATE.value + 1)
    elif y_int < 0:
        # move to beginning of previous line(s)
        codes += Cursor.PREV_LINE.template.format(abs(y_int))
    else:
        # move to beginning of next line(s)
        codes += Cursor.NEXT_LINE.template.format(y_int)
    # return movement
    return codes


def locate(x: int, y: Optional[int] = None) -> None:
    ansi_print(locate_code(x, y))


def locate_code(x: int, y: Optional[int] = None) -> str:
    x_int = int(x)
    y_int = None if y is None else int(y)
    min_x = Constant.MIN_X_COORDINATE.value
    min_y: Optional[int]
    if y is None:
        min_y = None
    else:
        min_y = Constant.MIN_Y_COORDINATE.value
    if x_int < min_x:
        msg = f"Invalid location x-coordinate, x >= {min_x}: {x}"
        raise ValueError(msg)
    if (y_int is not None) and (y_int < min_y):
        msg = f"Invalid location y-coordinate, y >= {min_y}: {y}"
        raise ValueError(msg)
    if y_int is None:
        # move to column on current row
        return Cursor.LOCATE_COLUMN.template.format(x_int + 1)
    else:
        # move to x, y coordinates
        return Cursor.LOCATE.template.format(y_int + 1, x_int + 1)


def get_cursor_location() -> Tuple[int, int]:
    response = _get_cursor_location_linux()
    location = response.split(";")
    col = int(location[1]) - 1
    row = int (location[0]) - 1
    return col, row


def _get_cursor_location_linux() -> str:
    response = ""
    # Send ANSI request for cursor location
    ansi_print(f"{Constant.CSI.value}6n")
    # Save terminal settings
    f = sys.stdin.fileno()
    terminal_settings = termios.tcgetattr(f)
    try:
        tty.setraw(f)
        # Skip first 2 bytes
        sys.stdin.read(2)
        # Read a single character
        char_in = sys.stdin.read(1)
        # Read until we reach a 'R' character
        while char_in != "R":
            response += char_in
            # Read a single character
            char_in = sys.stdin.read(1)
    finally:
        # Restore terminal settings
        termios.tcsetattr(f, termios.TCSADRAIN, terminal_settings)
    return response


########################################################################################################################
# Clear
########################################################################################################################

class Clear(Enum):
    SCREEN = Constant.CSI.value + "2J"
    TO_END_OF_SCREEN = Constant.CSI.value + "0J"
    TO_BEGIN_OF_SCREEN = Constant.CSI.value + "1J"
    LINE = Constant.CSI.value + "2K"
    TO_END_OF_LINE = Constant.CSI.value + "0K"
    TO_BEGIN_OF_LINE = Constant.CSI.value + "1K"

    def __init__(self, code):
        self.code = code


def clear_screen(arrow: str = "*", home: bool = False) -> None:
    ansi_print(clear_screen_code(arrow=arrow, home=home))


def clear_screen_code(arrow: str = "*", home: bool = False) -> str:
    codes = ""
    if arrow == ">":
        codes += Clear.TO_END_OF_SCREEN.code
    elif arrow == "<":
        codes += Clear.TO_BEGIN_OF_SCREEN.code
    else:
        codes += Clear.SCREEN.code
    if home:
        codes += locate_code(0, 0)
    return codes


def clear_line(arrow: str = "*", home: bool = False) -> None:
    ansi_print(clear_line_code(arrow=arrow, home=home))


def clear_line_code(arrow: str = "*", home: bool = False) -> str:
    codes = ""
    if arrow == ">":
        codes += Clear.TO_END_OF_LINE.code
    elif arrow == "<":
        codes += Clear.TO_BEGIN_OF_LINE.code
    else:
        codes += Clear.LINE.code
    if home:
        codes += locate_code(0, None)
    return codes


########################################################################################################################
# Screen
########################################################################################################################

def get_terminal_size() -> Tuple[int, int]:
    screen_size = shutil.get_terminal_size()
    return screen_size.columns, screen_size.lines


def ansi_print(text: str) -> None:
    print(text, end="", flush=True)


########################################################################################################################
# Utilities
########################################################################################################################

def hex_to_rgb(hex_code: str) -> Tuple[int, int, int]:
    hx_cd = hex_code.lstrip("#").strip()
    if not len(hx_cd) == Constant.HEX_CODE_RGB_CHAR_LENGTH.value:
        msg = f"Invalid hex code rgb length: {hex_code} == {len(hx_cd)}, "
        msg += f"len(hex_code) == {Constant.HEX_CODE_RGB_CHAR_LENGTH.value}"
        raise ValueError(msg)
    r = int(hx_cd[0:2], 16)
    g = int(hx_cd[2:4], 16)
    b = int(hx_cd[4:6], 16)
    return r, g, b


def hex_to_int(hex_code: str) -> int:
    hx_cd = hex_code.lstrip("#").strip()
    if not Constant.MIN_HEX_CODE_INT_CHAR_LENGTH.value <= len(hx_cd) <= Constant.MAX_HEX_CODE_INT_CHAR_LENGTH.value:
        msg = f"Invalid hex code integer length: {hex_code} == {len(hx_cd)}, "
        msg += f"{Constant.MIN_HEX_CODE_INT_CHAR_LENGTH.value} <= len(hex_code) <= "
        msg += f"{Constant.MAX_HEX_CODE_INT_CHAR_LENGTH.value}"
        raise ValueError(msg)
    return int(hx_cd, 16)


########################################################################################################################
# 256 Colors
########################################################################################################################

# TODO: add to highlight

def color256(fg: Optional[Union[str, int]] = None, bg: Optional[Union[str, int]] = None) -> str:
    codes = ""
    if fg is not None:
        codes += _get_256_code(index=fg, background=False)
    if bg is not None:
        codes += _get_256_code(index=bg, background=True)
    return codes


def _get_256_code(index: Union[int, str], background: bool) -> str:
    idx = hex_to_int(index) if isinstance(index, str) else index
    if Constant.MIN_HEX_COLOR.value <= idx <= Constant.MAX_HEX_COLOR.value:
        if idx == -1:
            return Color.DEFAULT.bg_code if background else Color.DEFAULT.fg_code
        else:
            cd = "48" if background else "38"
            return f"{Constant.CSI.value}{cd};5;{idx}m"
    else:
        msg = f"Invalid 256 "
        msg += "background" if background else "foreground"
        msg += f" color index: {index}, "
        msg += f"{Constant.MIN_HEX_COLOR.value} <= color_index <= {Constant.MAX_HEX_COLOR.value}"
        raise ValueError(msg)


########################################################################################################################
# Config
########################################################################################################################

# TODO: rename disabled to disable_highlights
# TODO: default colors need to handle 256, rgb modes (store as highlight dict? or just the string?)
# TODO: make this a singleton class


CONFIG = {"disabled": False,
          "default_fg": Color.DEFAULT,
          "default_bg": Color.DEFAULT}


def reset() -> None:
    enable()
    set_default_fg(Color.DEFAULT)
    set_default_bg(Color.DEFAULT)


def enable() -> None:
    CONFIG["disabled"] = False


def disable() -> None:
    CONFIG["disabled"] = True


def is_enabled() -> bool:
    return not CONFIG["disabled"]


def is_disabled() -> bool:
    return CONFIG["disabled"]


def set_default_colors(fg: Optional[Union[int, str, Color]] = None,
                       bg: Optional[Union[int, str, Color]] = None) -> None:
    if fg is not None:
        set_default_fg(fg)
    if bg is not None:
        set_default_bg(bg)


def set_default_fg(fg: Union[int, str, Color]) -> None:
    CONFIG["default_fg"] = fg if isinstance(fg, Color) else get_color_enum(value=fg)


def set_default_bg(bg: Union[int, str, Color]) -> None:
    CONFIG["default_bg"] = bg if isinstance(bg, Color) else get_color_enum(value=bg)


def get_default_fg() -> Color:
    return CONFIG["default_fg"]


def get_default_bg() -> Color:
    return CONFIG["default_bg"]
