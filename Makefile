SHELL := bash
MAKE_DIR := ./make
SOURCE_DIR := ./src
TESTS_DIR := ./test
DEMO_DIR := ./demo


.PHONY: help
help:
	@echo ""
	@cat "$(MAKE_DIR)/help.txt"
	@echo ""


.PHONY: demo
demo:
	@export PYTHONPATH="../src/" && cd "$(DEMO_DIR)" && python3 "color_index_chart_demo.py"
	@export PYTHONPATH="../src/" && cd "$(DEMO_DIR)" && python3 "color_name_demo.py"
	@export PYTHONPATH="../src/" && cd "$(DEMO_DIR)" && python3 "attribute_name_demo.py"


.PHONY: tests
tests:
	@cd "$(SOURCE_DIR)" && python3 -m pytest "../$(TESTS_DIR)"
